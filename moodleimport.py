import os
import openpyxl
import xml.etree.ElementTree as xmlTree
from TrueFalseQuestionType import TrueFalseQuestionType
from MatchingQuestionType import MatchingQuestionType
from MultiChoiceQuestionType import MultiChoiceQuestionType
from GapSelectQuestionType import GapSelectQuestionType

class MoodleImport():
    __present_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    __in_file = None
    __out_file = None
    __xslxObject = None
    SHEET_TRUE_FALSE = 0
    SHEET_MULTICHOICE = 1
    SHEET_MATCHING = 2
    SHEET_GAPSELECT = 3
    SHEET_FILLIN = 4
    SHEET_SHORT = 5
    SHEET_ESSAYS = 6

    def __init__(self, in_file_name, out_file_name):
        self.__in_file = os.path.join(self.__present_dir, in_file_name)
        self.__out_file = os.path.join(self.__present_dir, out_file_name)
        try:
            self.__xlsxObject = openpyxl.load_workbook(self.__in_file, True, False)
            self.sheets = self.__xlsxObject.sheetnames
            self.tfQTypes = TrueFalseQuestionType(self.__in_file, self.SHEET_TRUE_FALSE)
            self.matchTypes = MatchingQuestionType(self.__in_file, self.SHEET_MATCHING)
            self.multiTypes = MultiChoiceQuestionType(self.__in_file, self.SHEET_MULTICHOICE)
            self.gapSelectTypes = GapSelectQuestionType(self.__in_file, self.SHEET_GAPSELECT)
        except Exception as ex:
            print(ex)
            exit(2)

    def __setGroup(self):
        question = xmlTree.Element("question")
        question.set("type", "category")
        cat = self.tfQTypes.setQuestionNode(question, "category", "")
        self.tfQTypes.setQuestionNode(cat, "text", '$course$/top/Default for Love India')
        node = self.tfQTypes.setQuestionNode(question, "info", None, [["format", "moodle_auto_format"]])
        self.tfQTypes.setQuestionNode(node, "text", 'The default category for questions shared in context Love India.')
        self.tfQTypes.setQuestionNode(question, "idnumber", "")
        return question

    def generateXML(self):
        root = xmlTree.Element("quiz")
        root.append(self.__setGroup())

        for qObj in (self.tfQTypes.getQuestions()):
            root.append(qObj)

        for qObj in (self.matchTypes.getQuestions()):
            root.append(qObj)

        for qObj in (self.multiTypes.getQuestions()):
            root.append(qObj)

        for qObj in (self.gapSelectTypes.getQuestions()):
           root.append(qObj)

        tree = xmlTree.ElementTree(root)
        fobj = open(self.__out_file, "w")
        fobj.write("<?xml version=\"1.0\" ?> \n")
        fobj.close()
        with open (self.__out_file, "ab") as files :
            tree.write(files)

if __name__ == "__main__":
    importer = MoodleImport("input.xlsx", "sample.xml")
    importer.generateXML()

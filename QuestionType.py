import openpyxl
import xml.etree.ElementTree as xmlTree

class QuestionType():

    def __init__(self, in_file, sheet_number):
        self.in_file = in_file
        self.sheet_number = sheet_number

    def generateGeneralAnswerNode(self, question, answerText, isCorrect = True, score = "100" ):
        answerNode = xmlTree.SubElement(question, "answer")
        answerNode.set("fraction", score if (isCorrect) else "0")
        answerNode.set("format", "moodle_auto_format")
        answerTextNode = xmlTree.SubElement(answerNode, "text")
        answerTextNode.text = answerText
        answerFeedBackNode = xmlTree.SubElement(answerNode, "feedback")
        answerFeedBackNode.set("format", "html")
        answerFeedBackTextNode = xmlTree.SubElement(answerFeedBackNode, "text")
        answerFeedBackTextNode.text = "Correct!" if (isCorrect) else "In Correct!"
        return answerNode

    def setFeedBacks(self, question):
        node = self.setQuestionNode(question, "correctfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(node, "text", "Your answer is correct.")
        node = self.setQuestionNode(question, "partiallycorrectfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(node, "text", "Your answer is partially correct.")
        node = self.setQuestionNode(question, "incorrectfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(node, "text", "Your answer is incorrect.")
        self.setQuestionNode(question, "shownumcorrect", "")

    def generateBoolAnswerNode(self, question, trueState = True, isCorrect = True ):
        answerNode = xmlTree.SubElement(question, "answer")
        answerNode.set("fraction", "100" if (isCorrect) else "0")
        answerNode.set("format", "moodle_auto_format")
        answerTextNode = xmlTree.SubElement(answerNode, "text")
        answerTextNode.text = "true" if (trueState) else "false"
        answerFeedBackNode = xmlTree.SubElement(answerNode, "feedback")
        answerFeedBackNode.set("format", "html")
        answerFeedBackTextNode = xmlTree.SubElement(answerFeedBackNode, "text")
        answerFeedBackTextNode.text = "Correct!" if (trueState) else "In Correct!"
        return answerNode

    def setQuestionNode(self, question, label, text, attrs = []):
        questionTextNode = xmlTree.SubElement(question, label)
        if(text != None):
            questionTextNode.text = text
        for attr in attrs:
            questionTextNode.set(attr[0], attr[1])
        return questionTextNode

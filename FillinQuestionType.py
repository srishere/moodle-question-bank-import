from openpyxl import load_workbook
import xml.etree.ElementTree as xmlTree
from QuestionType import QuestionType

class FillinQuestionType(QuestionType):

    def __init__(self, in_file, sheet_number):
        QuestionType.__init__(self, in_file, sheet_number)

    def __parse(self):
        workbook = load_workbook(self.in_file)
        sheet = workbook.worksheets[self.sheet_number]
        i = 2
        question_label = sheet["A"+str(i)].value
        question_text = sheet["B"+str(i)].value
        dat = []
        while(question_label != None):
            row = { "question_label" : question_label, "question_text" : question_text }
            dat.append(row)
            i+=1
            question_label = sheet["A"+str(i)].value
            question_text = sheet["B"+str(i)].value
        return dat

    def getQuestions(self):
        data = self.__parse()
        ret = []
        for dat in data:
            ret.append(self.__generateFillInBlanksNode( "Fill in the blanks",
                                                    dat["question_text"].strip(),
                                                    "1",
                                                    "0"
                                                )
                        )
        return ret

    def __generateFillInBlanksNode(self, question_label, question_text, default_grade, penalty):
        question_text = "<![CDATA[<p dir='ltr' style='text-align: left;'>"+question_text+"</p>]]>"
        question = xmlTree.Element("question")
        question.set("type", "gapfill")
        nameNode = self.setQuestionNode(question, "name", "")
        self.setQuestionNode(nameNode, "text", question_label)
        node = self.setQuestionNode(question, "questiontext", None, [["format", "html"]])
        self.setQuestionNode(node, "text", question_text)
        node = self.setQuestionNode(question, "generalfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(question, "defaultgrade", default_grade)
        self.setQuestionNode(question, "penalty", penalty)
        self.setQuestionNode(question, "hidden", "0")
        self.setQuestionNode(question, "idnumber", "")
        self.setQuestionNode(question, "noduplicates", "0")
        self.setQuestionNode(question, "casesensitive", "0")
        self.setQuestionNode(question, "disableregex", "1")
        self.setQuestionNode(question, "fixedgapsize", "1")
        self.setQuestionNode(question, "optionsaftertext", "0")
        self.setQuestionNode(question, "letterhints", "0")
        self.setQuestionNode(question, "singleuse", "0")
        self.setQuestionNode(question, "delimitchars", "[]")
        self.setQuestionNode(question, "answerdisplay", "gapfill")

        self.setFeedBacks(question)

        answerNode = xmlTree.SubElement(question, "answer")
        answerNode.set("fraction", "1")
        answerNode.set("format", "moodle_auto_format")
        answerTextNode = xmlTree.SubElement(answerNode, "text")
        answerTextNode.text = ""
        answerFeedBackNode = xmlTree.SubElement(answerNode, "feedback")
        answerFeedBackNode.set("format", "html")
        answerFeedBackTextNode = xmlTree.SubElement(answerFeedBackNode, "text")
        answerFeedBackTextNode.text = ""

        return question

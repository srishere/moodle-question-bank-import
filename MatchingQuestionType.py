from openpyxl import load_workbook
import xml.etree.ElementTree as xmlTree
from QuestionType import QuestionType

class MatchingQuestionType(QuestionType):

    def __init__(self, in_file, sheet_number):
        QuestionType.__init__(self, in_file, sheet_number)

    def __getDataRow(self, i, sheet):
        question_text = sheet["A"+str(i)].value
        correct_answer = sheet["B"+str(i)].value
        default_grade = sheet["C"+str(i)].value
        penalty = sheet["D"+str(i)].value
        sub_questions = []
        if(sheet["E"+str(i)].value != None):
            sq_str_list = sheet["E"+str(i)].value.split("|")
            for ans in sq_str_list:
                if(ans != ""):
                    sub_questions.append(ans.split(",", 1))

        if(question_text == None):
            return None
        else:
            return { "question_text" : question_text.strip(), "default_grade" : default_grade, "penalty" : penalty, "sub_questions" : sub_questions }

    def __parse(self):
        workbook = load_workbook(self.in_file)
        sheet = workbook.worksheets[self.sheet_number]
        i = 2
        row = self.__getDataRow(i, sheet)
        dat = []
        while(row != None):
            dat.append(row)
            i+=1
            row = self.__getDataRow(i, sheet)
        return dat

    def getQuestions(self):
        data = self.__parse()
        ret = []
        for dat in data:
            #print(dat)
            ret.append(self.__generateMatchingNode( "Match The Following",
                                                    "Match The Following",
                                                    str(int(dat['default_grade'])).strip(),
                                                    str(int(dat['penalty'])).strip(),
                                                    dat['sub_questions']
                                                )
                        )
        return ret

    def __generateMatchingNode(self, question_label, question_text, default_grade, penalty, sub_questions):
        question = xmlTree.Element("question")
        question.set("type", "matching")
        nameNode = self.setQuestionNode(question, "name", "")
        self.setQuestionNode(nameNode, "text", question_label)
        node = self.setQuestionNode(question, "questiontext", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(node, "text", question_text)
        node = self.setQuestionNode(question, "generalfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(question, "defaultgrade", default_grade)
        self.setQuestionNode(question, "penalty", penalty)
        self.setQuestionNode(question, "hidden", "0")
        self.setQuestionNode(question, "idnumber", "")
        self.setQuestionNode(question, "shuffleanswers", "true")

        self.setFeedBacks(question)

        for sq in sub_questions:
            sub_question = self.setQuestionNode(question, "subquestion", None, [["format", "moodle_auto_format"]])
            self.setQuestionNode(sub_question, "text", sq[0].strip())
            aq_answer = self.setQuestionNode(sub_question, "answer", "")
            self.setQuestionNode(aq_answer, "text", sq[1].strip())

        return question

from openpyxl import load_workbook
import xml.etree.ElementTree as xmlTree
import operator

from QuestionType import QuestionType

class GapSelectQuestionType(QuestionType):

    def __init__(self, in_file, sheet_number):
        QuestionType.__init__(self, in_file, sheet_number)

    def extractAnswers(self, answer_rows):
        answers = []
        for row in answer_rows:
            answers.append(row.split(","))

        for row in answers:
            row[0] = row[0].strip()
            row[1] = row[1].strip()

        return sorted(answers, key = operator.itemgetter(1), reverse=True)

    def __parse(self):
        workbook = load_workbook(self.in_file)
        sheet = workbook.worksheets[self.sheet_number]
        i = 2
        question_label = sheet["A"+str(i)].value
        question_text = sheet["B"+str(i)].value
        answers = self.extractAnswers(sheet["C"+str(i)].value.split("|"))
        dat = []
        while(question_label != None and question_text != None):
            row = { "question_label" : question_label, "question_text" : question_text, "answers" : answers }
            dat.append(row)
            i+=1
            question_label = sheet["A"+str(i)].value
            question_text = sheet["B"+str(i)].value
            ansstr = sheet["C"+str(i)].value
            if(ansstr != None):
                ansstr = ansstr.split("|")
                answers = self.extractAnswers(ansstr)
        return dat

    def getQuestions(self):
        data = self.__parse()
        ret = []
        for dat in data:
            ret.append(self.__generateGapSelectNode( "Fill in the blanks",
                                                    dat["question_text"].strip(),
                                                    dat["answers"],
                                                    "1",
                                                    "0"
                                                )
                        )
        return ret

    def __generateGapSelectNode(self, question_label, question_text, answers, default_grade, penalty):
        question_text = "<![CDATA[<p dir='ltr' style='text-align: left;'>"+question_text+"</p>]]>"
        question = xmlTree.Element("question")
        question.set("type", "gapselect")
        nameNode = self.setQuestionNode(question, "name", "")
        self.setQuestionNode(nameNode, "text", question_label)
        node = self.setQuestionNode(question, "questiontext", None, [["format", "html"]])
        self.setQuestionNode(node, "text", question_text)
        node = self.setQuestionNode(question, "generalfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(question, "defaultgrade", default_grade)
        self.setQuestionNode(question, "penalty", penalty)
        self.setQuestionNode(question, "hidden", "0")
        self.setQuestionNode(question, "idnumber", "")
        self.setQuestionNode(question, "shuffleanswers", "1")
        self.setQuestionNode(question, "casesensitive", "0")
        self.setQuestionNode(question, "disableregex", "1")
        self.setQuestionNode(question, "singleuse", "0")

        self.setFeedBacks(question)

        for ans in answers:
            answerNode = xmlTree.SubElement(question, "selectoption")
            answerTextNode = xmlTree.SubElement(answerNode, "text")
            answerTextNode.text = ans[0]
            answerFeedBackNode = xmlTree.SubElement(answerNode, "group")
            answerFeedBackNode.text = "1"
        return question

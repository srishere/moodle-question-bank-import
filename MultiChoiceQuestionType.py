from openpyxl import load_workbook
import xml.etree.ElementTree as xmlTree
from QuestionType import QuestionType

class MultiChoiceQuestionType(QuestionType):

    def __init__(self, in_file, sheet_number):
        QuestionType.__init__(self, in_file, sheet_number)

    def __getDataRow(self, i, sheet):
        question_label = sheet["A"+str(i)].value
        question_text = sheet["B"+str(i)].value
        default_grade = sheet["C"+str(i)].value
        penalty = sheet["D"+str(i)].value
        sub_questions = []
        if(sheet["E"+str(i)].value != None):
            sq_str_list = sheet["E"+str(i)].value.split("|")
            for ans in sq_str_list:
                if(ans != ""):
                    sub_questions.append(ans.split(",", 1))

        if(question_text == None):
            return None
        else:
            return { "question_label" : question_label, "question_text" : question_label + "\n" + question_text, "default_grade" : default_grade, "penalty" : penalty, "sub_questions" : sub_questions }

    def __parse(self):
        workbook = load_workbook(self.in_file)
        sheet = workbook.worksheets[self.sheet_number]
        i = 2
        row = self.__getDataRow(i, sheet)
        dat = []
        while(row != None):
            dat.append(row)
            i+=1
            row = self.__getDataRow(i, sheet)
        return dat

    def getQuestions(self):
        ret = []
        data = self.__parse()
        for dat in data:
            ret.append(self.__generateMultiChoiceNode( dat["question_label"].strip(),
                                                    dat["question_text"].strip(),
                                                    str(int(dat['default_grade'])).strip(),
                                                    str(int(dat['penalty'])).strip(),
                                                    dat["sub_questions"]
                                                )
                        )
        return ret

    def __generateMultiChoiceNode(self, question_label, question_text, default_grade, penalty, answers_list):
        question = xmlTree.Element("question")
        question.set("type", "multichoice")
        nameNode = self.setQuestionNode(question, "name", "")
        self.setQuestionNode(nameNode, "text", question_label)
        node = self.setQuestionNode(question, "questiontext", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(node, "text", question_text)
        node = self.setQuestionNode(question, "generalfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(question, "defaultgrade", default_grade)
        self.setQuestionNode(question, "penalty", penalty)
        self.setQuestionNode(question, "hidden", "0")
        self.setQuestionNode(question, "idnumber", "")
        self.setQuestionNode(question, "single", "false")
        self.setQuestionNode(question, "shuffleanswers", "true")
        self.setQuestionNode(question, "answernumbering", "iii")
        self.setQuestionNode(question, "showstandardinstruction", "0")

        self.setFeedBacks(question)
        score = 100
        correct_count = 0
        for ans in answers_list:
            correct_flag = bool(ans[1].strip().upper() == "TRUE" or ans[1].strip() == "1")
            if(correct_flag):
                correct_count += 1
        if(correct_count > 0):
            score = 100.00 / float(correct_count)
        for ans in answers_list:
            correct_flag = bool(ans[1].strip().upper() == "TRUE" or ans[1].strip() == "1")
            self.generateGeneralAnswerNode(question, ans[0].strip(), correct_flag, str(score) )
        return question

from openpyxl import load_workbook
import xml.etree.ElementTree as xmlTree
from QuestionType import QuestionType

class TrueFalseQuestionType(QuestionType):

    def __init__(self, in_file, sheet_number):
        QuestionType.__init__(self, in_file, sheet_number)

    def __parse(self):
        workbook = load_workbook(self.in_file)
        sheet = workbook.worksheets[self.sheet_number]
        i = 2
        question_text = sheet["B"+str(i)].value
        answer_text = sheet["C"+str(i)].value.strip().upper()
        correct_answer = bool(answer_text == "TRUE" or answer_text == "1")
        dat = []
        while(question_text != None):
            row = { "question_text" : question_text, "correct_answer" : bool(correct_answer) }
            dat.append(row)
            i+=1
            question_text = sheet["B"+str(i)].value
            if(sheet["C"+str(i)].value != None):
                answer_text = sheet["C"+str(i)].value.strip().upper()
                correct_answer = bool(answer_text == "TRUE" or answer_text == "1")
            else:
                correct_answer = None
        return dat

    def getQuestions(self):
        data = self.__parse()
        ret = []
        for dat in data:
            ret.append(self.__generateTrueFalseNode(dat['question_text'].strip(), dat['correct_answer']))
        return ret

    def __generateTrueFalseNode(self, question_text, trueIsCorrect = True):
        question = xmlTree.Element("question")
        question.set("type", "truefalse")

        node = self.setQuestionNode(question, "questiontext", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(node, "text", question_text)

        nameNode = self.setQuestionNode(question, "name", "")
        self.setQuestionNode(nameNode, "text", 'Please select true or false.')

        node = self.setQuestionNode(question, "generalfeedback", None, [["format", "moodle_auto_format"]])
        self.setQuestionNode(node, "text", '')
        self.setQuestionNode(question, "defaultgrade", "1.0000000")
        self.setQuestionNode(question, "penalty", "1.0000000")
        self.setQuestionNode(question, "hidden", "0")
        self.setQuestionNode(question, "idnumber", "")
        self.generateBoolAnswerNode(question, True, trueIsCorrect)
        self.generateBoolAnswerNode(question, False, not trueIsCorrect)
        return question


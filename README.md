[![Developed to meet a personal requirement. Shall expand to make this generic.](https://www.angeldrome.com/img/logo.png)](https://www.angeldrome.com/)
# Moodle Question Bank Import


Developed to meet a personal requirement. Shall expand to make this generic


Python based question bank importer for moodle.


Code is very rigid as now.


Need to make this totally configurable to deal with question bank types, import file types (right now expects only xlsx) and accomodate the other question types.


When fed with right xlsx file (no validation it will just fail), it will produce a moodle xml file to import.


Tested with moodle 3.9 version (not sure on anything prior or later)

A Sample XLSX file is there in the same directory (input.xlsx)
